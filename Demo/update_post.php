<?php
// Include files
require_once '../Database/Database.php';
require_once '../Database/PostDA.php';
require_once '../Models/Post.php';

// Set Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    if (!isset($_REQUEST['id'])) {
        echo "Please include an \"id\" parameter in the url. E.g. \"{$_SERVER['PHP_SELF']}?id=1\"";
        exit(0);
    }

    // Get request body data
    $data = json_decode(file_get_contents('php://input'));

    // Data validation
    if ($errMsg = PostDA::isSetCheck($data)) {
        echo json_encode($errMsg);
        exit(0);
    } else if ($errMsg = PostDA::validateData($data)) {
        echo json_encode($errMsg);
        exit(0);
    }

    // Get Configs
    $dbConfigs = Database::readConfigs('../db.json');

    // Instantiate new instances of Data Access object
    $postDA = new PostDA($dbConfigs->db_host, $dbConfigs->db_name, $dbConfigs->db_username, $dbConfigs->db_password, 'posts');

    // Return if no result
    if ($postDA->getSinglePost($_REQUEST['id'])->rowCount() === 0) {
        echo json_encode(array(
            'message' => 'The specified post is not found.',
        ));
        exit(0);
    }

    // Add the new post
    echo ($postDA->updatePost((array) $data, $_REQUEST['id'])) ? json_encode([
        'message' => 'Post successfully updated!',
    ]) : json_encode([
        'message' => 'Post was not successfully updated.',
    ]);
} else {
    echo json_encode([
        'message' => 'Method not allowed.',
    ]);
}

?>