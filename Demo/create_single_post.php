<?php
// Include files
require_once '../Database/Database.php';
require_once '../Database/PostDA.php';

// Set Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get request body data
    $data = json_decode(file_get_contents('php://input'));

    // Data validation
    if ($errMsg = PostDA::isSetCheck($data)) {
        echo json_encode($errMsg);
        exit(0);
    } else if ($errMsg = PostDA::validateData($data)) {
        echo json_encode($errMsg);
        exit(0);
    }

    // Get Configs
    $dbConfigs = Database::readConfigs('../db.json');

    // Instantiate new instances of Data Access object
    $postDA = new PostDA($dbConfigs->db_host, $dbConfigs->db_name, $dbConfigs->db_username, $dbConfigs->db_password, 'posts');

    // Add the new post
    echo ($postDA->createPost((array) $data)) ? json_encode([
        'message' => 'Post successfully created!'
    ]) : json_encode([
        'message' => 'Post was not successfully created.'
    ]);
} else {
    echo json_encode([
        'message' => 'Method not allowed.',
    ]);
}

?>