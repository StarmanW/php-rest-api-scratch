<?php
// Include files
require_once '../Database/Database.php';
require_once '../Database/PostDA.php';
require_once '../Database/CategoryDA.php';
require_once '../Models/Post.php';
require_once '../Models/Category.php';

// Get Configs
$dbConfigs = Database::readConfigs('../db.json');

// Set Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

// Instantiate new instances of Data Access object
$postDA = new PostDA($dbConfigs->db_host, $dbConfigs->db_name, $dbConfigs->db_username, $dbConfigs->db_password, 'posts');
$catDA = new CategoryDA($dbConfigs->db_host, $dbConfigs->db_name, $dbConfigs->db_username, $dbConfigs->db_password, 'categories');

// Get all post
$postsResult = $postDA->getPosts();

// Return if no result
if ($postsResult->rowCount() === 0) {
    echo json_encode(array(
        'message' => 'No posts found.',
    ));
    exit(0);
}

// Post array
$post_arr['posts'] = array();

// Loop through each result
while ($row = $postsResult->fetch(PDO::FETCH_ASSOC)) {
    // Retrieve category data
    $category = $catDA->getSingleCategory($row['category_id']);

    // Instantiate new post
    $post = new Post($row['id'], $row['title'], $row['body'], $row['author'], new Category($category['id'], $category['name'], $category['created_at']), $row['created_at']);

    // Push post object into array
    array_push($post_arr['posts'], array(
        'id' => $post->getId(),
        'title' => $post->getTitle(),
        'body' => $post->getBody(),
        'author' => $post->getAuthor(),
        'category' => array(
            'category_id' => $post->getCategory()->getId(),
            'category_name' => $post->getCategory()->getName(),
        ),
        'created_at' => $post->getCreatedAt(),
    ));
}

// Convert array data to JSON and output data
echo json_encode($post_arr);

?>