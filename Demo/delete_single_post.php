<?php
// Include files
require_once '../Database/Database.php';
require_once '../Database/PostDA.php';
require_once '../Models/Post.php';

// Set Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

if ($_SERVER['REQUEST_METHOD'] === "DELETE") {
    // Get Configs
    $dbConfigs = Database::readConfigs('../db.json');

    // Instantiate new instances of Data Access object
    $postDA = new PostDA($dbConfigs->db_host, $dbConfigs->db_name, $dbConfigs->db_username, $dbConfigs->db_password, 'posts');

    if ($postDA->getSinglePost($_REQUEST['id'])->rowCount() === 0) {
        echo json_encode(array(
            'message' => 'The specified post is not found.',
        ));
        exit(0);
    }

    echo ($postDA->deletePost($_REQUEST['id'])) ? json_encode(array(
        'message' => 'Post successfully deleted!',
    )) : json_encode(array(
        'message' => 'Post was not successfully deleted.',
    ));
} else {
    echo json_encode([
        'message' => 'Method not allowed.',
    ]);
}

?>