<?php

class PostDA extends Database {
    // PostDA properties
    private $table;

    // Constructor function
    public function __construct($host, $db_name, $username, $password, $table) {
        parent::__construct($host, $db_name, $username, $password);
        $this->initConn();
        $this->table = $table;
    }

    // Function to read all post from database
    public function getPosts() {
        // Prepared Statement
        $stmt = $this->conn->prepare("SELECT * FROM {$this->table}");
        $stmt->execute();
        return $stmt;
    }

    // Function to get single post
    public function getSinglePost($id) {
        // Prepared Statement
        $stmt = $this->conn->prepare("SELECT * FROM {$this->table} WHERE ID = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return $stmt;
    }

    // Function to create new post
    public function createPost($data) {
        // Sanitize data
        $title = htmlentities($data['title'], ENT_QUOTES, 'UTF-8');
        $body = htmlentities($data['body'], ENT_QUOTES, 'UTF-8');
        $author = htmlentities($data['author'], ENT_QUOTES, 'UTF-8');
        $category_id = htmlentities($data['category_id'], ENT_QUOTES, 'UTF-8');

        // Prepare SQL statement & bind parameters
        $stmt = $this->conn->prepare("INSERT INTO {$this->table} SET title = :title, body = :body, author = :author, category_id = :category_id");
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":body", $body);
        $stmt->bindParam(":author", $author);
        $stmt->bindParam(":category_id", $category_id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: %s \n", $stmt->error);
            return false;
        }
    }

    // Function to update new post
    public function updatePost($data, $id) {
        // Sanitize data
        $title = htmlentities($data['title'], ENT_QUOTES, 'UTF-8');
        $body = htmlentities($data['body'], ENT_QUOTES, 'UTF-8');
        $author = htmlentities($data['author'], ENT_QUOTES, 'UTF-8');
        $category_id = htmlentities($data['category_id'], ENT_QUOTES, 'UTF-8');

        // Prepare SQL statement & bind parameters
        $stmt = $this->conn->prepare("UPDATE {$this->table} SET title = :title, body = :body, author = :author, category_id = :category_id WHERE ID = :id");
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":body", $body);
        $stmt->bindParam(":author", $author);
        $stmt->bindParam(":category_id", $category_id);
        $stmt->bindParam(":id", $id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: %s \n", $stmt->error);
            return false;
        }
    }

    // Function to update new post
    public function deletePost($id) {
        // Prepare SQL statement & bind parameters
        $stmt = $this->conn->prepare("DELETE FROM {$this->table} WHERE ID = :id");
        $stmt->bindParam(":id", $id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: %s \n", $stmt->error);
            return false;
        }
    }

    // Validate post data
    public static function validateData($data) {
        $validation = array();
        
        // Valid format check
        if (!preg_match("/^[\w\W\s]+$/", $data->title)) {
            array_push($validation, [
                'title' => 'The title format is invalid.',
            ]);
        }

        if (!preg_match("/^[\w\W\s]+$/", $data->body)) {
            array_push($validation, [
                'body' => 'The body format is invalid.',
            ]);
        }

        if (!preg_match("/^[A-z\@\- ]{2,}$/", $data->author)) {
            array_push($validation, [
                'author' => 'The author format is invalid.',
            ]);
        }

        if (!preg_match("/^[0-9]+$/", $data->category_id)) {
            array_push($validation, [
                'category' => 'The category is invalid.',
            ]);
        }

        return (sizeof($validation) === 0) ? 0 : $validation ;
    }

    // Isset check
    public static function isSetCheck($data) {
        $validation = array();

        if (!isset($data->title)) {
            array_push($validation, [
                'title' => 'The title field is required.',
            ]);
        }

        if (!isset($data->body)) {
            array_push($validation, [
                'body' => 'The body field is required.',
            ]);
        }

        if (!isset($data->author)) {
            array_push($validation, [
                'author' => 'The author field is required.',
            ]);
        }

        if (!isset($data->category_id)) {
            array_push($validation, [
                'category' => 'The category field is required.',
            ]);
        }

        return (sizeof($validation) === 0) ? 0 : $validation ;
    }
}

?>