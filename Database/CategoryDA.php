<?php

class CategoryDA extends Database {

    // PostDA properties
    private $table;

    // Constructor function
    public function __construct($host, $db_name, $username, $password, $table) {
        parent::__construct($host, $db_name, $username, $password);
        $this->initConn();
        $this->table = $table;
    }

    // Function to read post from database
    public function getSingleCategory($id) {
        // Prepared Statement
        $stmt = $this->conn->prepare("SELECT * FROM {$this->table} WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}

?>