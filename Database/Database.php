<?php

class Database {

    // DB Properties
    private $host;
    private $db_name;
    private $username;
    private $password;
    protected $conn;

    // Public constructor function
    public function __construct($host, $db_name, $username, $password) {
        $this->host = $host;
        $this->db_name = $db_name;
        $this->username = $username;
        $this->password = $password;
    }

    // Initialize database connection
    public function initConn() {
        $this->conn = null;
        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            echo "Connection error: " . $ex->getMessage();
        }
    }

    // Read Configs from JSON file
    public static function readConfigs($path) {
        return json_decode(file_get_contents($path));
    }
}

?>