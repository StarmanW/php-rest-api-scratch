# Simple PHP REST API from scratch

## Setup
----
1. Rename `db.example.json` to `db.json` and provide all the required database information in the JSON file.
2. Import sample data from `myblog.sql` file into your database.

## API Endpoints
----
* `[GET]` /posts/ - Retrieve all posts
* `[GET]` /post/1 - Retrieve a specified post based on given ID
* `[POST]` /post/ - Create a new post
* `[PUT]` /post/1/update - Update a post based on given ID
* `[DELETE]` /post/1/delete - Delete a post based on given ID
