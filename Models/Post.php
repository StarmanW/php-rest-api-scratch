<?php

class Post {
    // Post properties
    private $id;
    private $title;
    private $body;
    private $author;
    private $category;
    private $createdAt;

    // Constructor function
    public function __construct($id, $title, $body, $author, $category, $createdAt) {
        $this->id = $id;
        $this->title = $title;
        $this->body = $body;
        $this->author = $author;
        $this->category = $category;
        $this->createdAt = $createdAt;
    }

    /** Getters function */
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getBody() {
        return $this->body;
    }
    public function getAuthor() {
        return $this->author;
    }
    public function getCategory() {
        return $this->category;
    }
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /** Setters function */
    public function setId($id) {
        $this->id = $id;
    }
    public function setTitle($title) {
        $this->title = $title;
    }
    public function setBody($body) {
        $this->body = $body;
    }
    public function setAuthor($author) {
        $this->author = $author;
    }
    public function setCategory($category) {
        $this->category = $category;
    }
}

?>