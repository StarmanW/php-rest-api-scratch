<?php

class Category {

    // Category properties
    private $id;
    private $name;
    private $createdAt;

    /** Constructor function */
    public function __construct($id, $name, $createdAt) {
        $this->id = $id;
        $this->name = $name;
        $this->createdAt = $createdAt;
    }

    /** Getters function */
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /** Setters function */
    public function setId($id) {
        $this->id = $id;
    }
    public function setName($name) {
        $this->name = $name;
    }
}

?>